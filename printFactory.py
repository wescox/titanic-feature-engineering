import numpy as np
import pandas as pd

import matplotlib.pyplot as plt


class PrintFactory:
	data = pd.DataFrame()

	def __init__(self, input_data):
		self.data = input_data.copy()

	def printTitles(self):
		data = self.data.copy()
		fig = plt.figure(figsize=(9,5))

		titles = data['Name'].str.split(',',expand=True)[1].str.strip().str.split('.',expand=True)[0]

		titleValues = titles.value_counts().keys().tolist()
		titleCounts = titles.value_counts().tolist()

		data['Title'] = titles

		print(titles.value_counts())

		plt.barh(range(0,len(titleValues)), titleCounts,0.5, align='center')
		plt.yticks(range(0,len(titleValues)), titleValues)
		plt.xlabel('Counts')
		plt.title('Passenger Title Counts')
		plt.show()

		fig.savefig('./titanic_TitleCounts.png')
		plt.close()
		# Comment: Master, Mrs, Miss and Mr are the 4 biggest titles. All the others are less than 10 examples

		fig = plt.figure(figsize=(9,5))
		data['AgeRange'] = pd.cut(data['Age'], [0,10,20,30,40,50,60,70,80,90], include_lowest=True)

		commonTitles = ['Mr','Miss','Mrs','Master']

		titleMr_ageCount = data[data['Title'] == 'Mr']['AgeRange'].value_counts().sort_index()
		titleMiss_ageCount = data[data['Title'] == 'Miss']['AgeRange'].value_counts().sort_index()
		titleMrs_ageCount = data[data['Title'] == 'Mrs']['AgeRange'].value_counts().sort_index()
		titleMaster_ageCount = data[data['Title'] == 'Master']['AgeRange'].value_counts().sort_index()
		titleRare_ageCount = data[~data['Title'].isin(commonTitles)]['AgeRange'].value_counts().sort_index()
		print(titleMr_ageCount)
		print(len(titleMr_ageCount))


		width = 0.1

		plt.bar(np.arange(0,len(titleMr_ageCount))-2*width, titleMr_ageCount,width, align='center')
		plt.bar(np.arange(0,len(titleMiss_ageCount))-width, titleMiss_ageCount,width, align='center')
		plt.bar(np.arange(0,len(titleMrs_ageCount)), titleMrs_ageCount,width, align='center')
		plt.bar(np.arange(0,len(titleMaster_ageCount))+width, titleMaster_ageCount,width, align='center')
		plt.bar(np.arange(0,len(titleRare_ageCount))+2*width, titleRare_ageCount,width, align='center')
		plt.xticks(range(0,len(titleMr_ageCount)), titleMr_ageCount.keys())
		plt.ylabel('Counts')
		plt.xlabel('Age')
		plt.title('Passenger Titles by Age')
		plt.legend(['Mr','Miss','Mrs','Master','Rare'])
		plt.show()
		fig.savefig('./titanic_TitleCountsByAge.png')
		# Comment: The distribution of passenger titles varies by age. Add 5 categories for each title type.

	def printParch(self):

		data = self.data.copy()
		fig = plt.figure()

		died_male = pd.DataFrame(data[(data['Sex'] == 'male') & (data['Survived'] == 0)]['Parch'].value_counts().sort_index())
		survived_male = pd.DataFrame(data[(data['Sex'] == 'male') & (data['Survived'] == 1)]['Parch'].value_counts().sort_index())

		died_female = pd.DataFrame(data[(data['Sex'] == 'female') & (data['Survived'] == 0)]['Parch'].value_counts().sort_index())
		survived_female = pd.DataFrame(data[(data['Sex'] == 'female') & (data['Survived'] == 1)]['Parch'].value_counts().sort_index())

		# Fill out the missing rows with zeros
		died_male["A"] = died_male.index
		survived_male["A"] = survived_male.index
		died_female["A"] = died_female.index
		survived_female["A"] = survived_female.index

		new_index = pd.Index(np.arange(0,data['Parch'].max() + 1), name="A")

		died_male = died_male.set_index("A").reindex(new_index).reset_index().fillna(0)
		survived_male = survived_male.set_index("A").reindex(new_index).reset_index().fillna(0)
		died_female = died_female.set_index("A").reindex(new_index).reset_index().fillna(0)
		survived_female = survived_female.set_index("A").reindex(new_index).reset_index().fillna(0)

		cummulative = survived_male['Parch']*0
		plt.bar(np.arange(0,data['Parch'].max() + 1),survived_male['Parch'],0.5, bottom=cummulative, color=(0.0,0.0,1.0))
		cummulative += survived_male['Parch']
		plt.bar(np.arange(0,data['Parch'].max() + 1),survived_female['Parch'],0.5,bottom=cummulative, color=(1.0,0.41,0.7))
		cummulative += survived_female['Parch']
		plt.bar(np.arange(0,data['Parch'].max() + 1),died_male['Parch'],0.5,bottom=cummulative, color=(0.53,0.81,0.98))
		cummulative += died_male['Parch']
		plt.bar(np.arange(0,data['Parch'].max() + 1),died_female['Parch'],0.5,bottom=cummulative, color=(1.0,0.75,0.8))

		plt.legend(['Survived Male','Survived Female','Died Male','Died Female'])
		plt.xlabel('Parch')
		plt.ylabel('Count')
		plt.title('Parch Survival')

		plt.show()

		# fig.savefig('./titanic_ParchSurvival.png')

		# Comments: Alone is an important dictator for male and female for SibSp, 
		# one other parch is also important. Beyond that, not so much.
		# Result: Create parameters: Alone, Single Parch, MoreThan2

	def printSibSp(self):

		data = self.data.copy()
		fig = plt.figure()

		died_male = pd.DataFrame(data[(data['Sex'] == 'male') & (data['Survived'] == 0)]['SibSp'].value_counts().sort_index())
		survived_male = pd.DataFrame(data[(data['Sex'] == 'male') & (data['Survived'] == 1)]['SibSp'].value_counts().sort_index())

		died_female = pd.DataFrame(data[(data['Sex'] == 'female') & (data['Survived'] == 0)]['SibSp'].value_counts().sort_index())
		survived_female = pd.DataFrame(data[(data['Sex'] == 'female') & (data['Survived'] == 1)]['SibSp'].value_counts().sort_index())

		# Fill out the missing rows with zeros
		died_male["A"] = died_male.index
		survived_male["A"] = survived_male.index
		died_female["A"] = died_female.index
		survived_female["A"] = survived_female.index

		new_index = pd.Index(np.arange(0,data['SibSp'].max() + 1), name="A")

		died_male = died_male.set_index("A").reindex(new_index).reset_index().fillna(0)
		survived_male = survived_male.set_index("A").reindex(new_index).reset_index().fillna(0)
		died_female = died_female.set_index("A").reindex(new_index).reset_index().fillna(0)
		survived_female = survived_female.set_index("A").reindex(new_index).reset_index().fillna(0)

		cummulative = survived_male['SibSp']*0
		plt.bar(np.arange(0,data['SibSp'].max() + 1),survived_male['SibSp'],0.5, bottom=cummulative, color=(0.0,0.0,1.0))
		cummulative += survived_male['SibSp']
		plt.bar(np.arange(0,data['SibSp'].max() + 1),survived_female['SibSp'],0.5,bottom=cummulative, color=(1.0,0.41,0.7))
		cummulative += survived_female['SibSp']
		plt.bar(np.arange(0,data['SibSp'].max() + 1),died_male['SibSp'],0.5,bottom=cummulative, color=(0.53,0.81,0.98))
		cummulative += died_male['SibSp']
		plt.bar(np.arange(0,data['SibSp'].max() + 1),died_female['SibSp'],0.5,bottom=cummulative, color=(1.0,0.75,0.8))

		plt.legend(['Survived Male','Survived Female','Died Male','Died Female'])
		plt.xlabel('SibSp')
		plt.ylabel('Count')
		plt.title('SibSp Survival')

		plt.show()
		# fig.savefig('./titanic_SibSpSurvival.png')

		# Comments: Alone is an important dictator for men and women for SibSp, 
		# one other partner is also important. Beyond that, not so much.
		# Result: Create parameters: Alone, Single SibSp, MoreThan2

	def printPassengersOnTicket(self):

		data = self.data.copy()
		fig = plt.figure()

		# Percentage of Ticketholders survived parameter
		data['PassengersOnTicket'] = data.groupby('Ticket')['Ticket'].transform('count')
		data['SurvivorsOnTicket'] = data.groupby('Ticket')['Survived'].transform('sum')

		data['PercentTicketSurvived'] = data['SurvivorsOnTicket']/data['PassengersOnTicket']

		# the histogram of the data
		passengers1, bins, patches = plt.hist(data[data['PassengersOnTicket'] == 1]['PercentTicketSurvived'], bins=21)
		passengers2, bins, patches = plt.hist(data[data['PassengersOnTicket'] == 2]['PercentTicketSurvived'], bins=21)
		passengers3, bins, patches = plt.hist(data[data['PassengersOnTicket'] == 3]['PercentTicketSurvived'], bins=21)
		passengers4, bins, patches = plt.hist(data[data['PassengersOnTicket'] == 4]['PercentTicketSurvived'], bins=21)
		passengers5, bins, patches = plt.hist(data[data['PassengersOnTicket'] == 5]['PercentTicketSurvived'], bins=21)
		passengers6, bins, patches = plt.hist(data[data['PassengersOnTicket'] == 6]['PercentTicketSurvived'], bins=21)
		passengers7, bins, patches = plt.hist(data[data['PassengersOnTicket'] == 7]['PercentTicketSurvived'], bins=21)
		plt.close()
		combined = np.append([passengers1],[passengers2], axis=0)
		combined = np.append(combined,[passengers3], axis=0)
		combined = np.append(combined,[passengers4], axis=0)
		combined = np.append(combined,[passengers5], axis=0)
		combined = np.append(combined,[passengers6], axis=0)
		combined = np.append(combined,[passengers7], axis=0)

		cummulative = passengers1*0
		plt.bar(np.arange(0.0,1.05,0.05),passengers1,0.03,bottom=cummulative)
		cummulative += passengers1
		plt.bar(np.arange(0.0,1.05,0.05),passengers2,0.03,bottom=cummulative)
		cummulative += passengers2
		plt.bar(np.arange(0.0,1.05,0.05),passengers3,0.03,bottom=cummulative)
		cummulative += passengers3
		plt.bar(np.arange(0.0,1.05,0.05),passengers4,0.03,bottom=cummulative)
		cummulative += passengers4
		plt.bar(np.arange(0.0,1.05,0.05),passengers5,0.03,bottom=cummulative)
		cummulative += passengers5
		plt.bar(np.arange(0.0,1.05,0.05),passengers6,0.03,bottom=cummulative)
		cummulative += passengers6
		plt.bar(np.arange(0.0,1.05,0.05),passengers7,0.03,bottom=cummulative)

		plt.legend(['1 Passenger','2 Passengers','3 Passengers','4 Passengers','5 Passengers','6 Passengers','7 Passengers'])
		plt.title('Passengers on Same Ticket')
		plt.ylabel('Count')
		plt.xlabel('Percent Survivors on Ticket')

		# print(combined.T)

		plt.show()
		# fig.savefig('./titanic_PercentSurvivorsOnTicket.png')