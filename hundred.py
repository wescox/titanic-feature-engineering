import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from sklearn import svm

from ageEstimator import AgeEstimator
from printFactory import PrintFactory
from models import Models

printTheData = False
estimateTheAge = True
fitTheModel_SVM = True

# Import the data
train_data = pd.read_csv('train.csv', sep=',',header=0)

train_target = train_data['Survived']

excluded_features = ['Survived','PassengerId','Sex','Ticket','SecondClass','Cabin','Embarked','Name','Title']


train_data['Male'] = np.where(train_data['Sex']=='male', 1, 0)

train_data['FirstClass'] = np.where(train_data['Pclass']==1, 1, 0)
train_data['SecondClass'] = np.where(train_data['Pclass']==2, 1, 0)
train_data['ThirdClass'] = np.where(train_data['Pclass']==3, 1, 0)

train_data['ParchAlone'] = np.where(train_data['Parch']==0, 1, 0)
train_data['ParchPair'] = np.where(train_data['Parch']==1, 1, 0)
train_data['ParchMore'] = np.where(train_data['Parch']>1, 1, 0)

train_data['SibSpAlone'] = np.where(train_data['SibSp']==0, 1, 0)
train_data['SibSpPair'] = np.where(train_data['SibSp']==1, 1, 0)
train_data['SibSpMore'] = np.where(train_data['SibSp']>1, 1, 0)

train_data['Title'] = train_data['Name'].str.split(',',expand=True)[1].str.strip().str.split('.',expand=True)[0]
train_data['TitleMr'] = np.where(train_data['Title']=='Mr', 1, 0)
train_data['TitleMiss'] = np.where(train_data['Title']=='Miss', 1, 0)
train_data['TitleMrs'] = np.where(train_data['Title']=='Mrs', 1, 0)
train_data['TitleMaster'] = np.where(train_data['Title']=='Master', 1, 0)
train_data['TitleRare'] = np.where(train_data['Title']=='Rare', 1, 0)

if printTheData:
	printFactory = PrintFactory(train_data)
	printFactory.printPassengersOnTicket()
	printFactory.printSibSp()
	printFactory.printParch()
	printFactory.printTitles()

if estimateTheAge:
	estimator = AgeEstimator(train_data)
	estimated_age = estimator.estimate(excluded_features)

	print(estimated_age.describe())
	train_data['Age'] = estimated_age

if fitTheModel_SVM:
	train_data.drop(excluded_features, axis=1, inplace=True)
	train_data.drop(['Fare','Pclass'], axis=1, inplace=True)

	models = Models()
	models.fitSVM(train_data, train_target)