import numpy as np
import pandas as pd
from sklearn import linear_model

class AgeEstimator:
	data = pd.DataFrame()

	def __init__(self, input_data):
		self.data = input_data.copy()
		# print("Estimator create")

	def estimate(self, excluded_features):
		self.data.drop(excluded_features, axis=1, inplace=True)
		# print(self.data.info())

		missing_data = self.data[self.data['Age'].isnull()].copy()
		full_data = self.data[self.data['Age'].notnull()]

		# print("Full " + str(full_data.shape))
		# print("Miss " + str(missing_data.shape))
		# print(missing_data.head())
		# self.data.loc[self.data["Name"] == "Braund, Mr. Owen Harris", "Age"] = 99.0

		lin = linear_model.LinearRegression(fit_intercept=True, normalize=True, copy_X=True)
		print(lin.fit(full_data.drop(['Age'],axis=1), full_data['Age']))

		missing_data['Age'] = lin.predict(missing_data.drop(['Age'],axis=1))

		# print(full_data['Age'].describe())
		# print(missing_data['Age'].describe())

		return pd.concat([full_data,missing_data]).sort_index()['Age']