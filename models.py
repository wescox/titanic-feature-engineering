import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from sklearn import svm

class Models:

	def normalize_features(self, input_data):
		data = input_data.copy()
		# Only scale the columns our algorithm will be learning from
		cols = ['Age']

		mean = np.sum(data[cols], axis=0)/data[cols].count()

		data[cols] = data[cols] - mean

		# Calculate the L2 norm for each column
		l2norm = np.sqrt(np.sum(np.square(data[cols]), axis=0))
		# Normalize the data
		normalized_train_data = data.copy()
		normalized_train_data[cols] = normalized_train_data[cols].divide(l2norm)

		return normalized_train_data, l2norm

	def fitSVM(self, train_data, target_data):

		# print(train_data.head())

		normalized_data, l2norm = self.normalize_features(train_data)
		# print(normalized_data.head())

		C_penalty = 100000
		gamma = 50
		lin_clf = svm.SVC(kernel='rbf', C=C_penalty, gamma=gamma)
		print(lin_clf.fit(normalized_data, target_data))

		score = lin_clf.score(normalized_data, target_data)
		print("Score " + str(score))